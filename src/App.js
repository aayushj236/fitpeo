import { useState } from "react";
import Sidebar from "./Utils/Sidebar";
import Dashboard from "./Components/Dashboard";

const App = () => {

  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <div>
        <div className="flex h-screen bg-slate-300 overflow-hidden">
          <Sidebar
            sidebarOpen={sidebarOpen}
            setSidebarOpen={setSidebarOpen}
          />
          <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
            <main className="no-scrollbar overflow-auto">
              <div className="px-3 pt-6  pb-10 w-full max-w-10xl mx-auto">
                <Dashboard  
                 sidebarOpen={sidebarOpen}
                 setSidebarOpen={setSidebarOpen}
                />
              </div>
            </main>
          </div>
        </div>
    </div>
  );
}

export default App;