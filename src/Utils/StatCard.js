import { Avatar } from '@chakra-ui/react'
import React from 'react'
import { AiOutlineArrowUp, AiOutlineArrowDown } from 'react-icons/ai'

const StatCard = ({AvatarBg,AvatarIcon,FirstLine,SecondLine,Percentage,PercentCriteria}) => {
  return (
    <div>
        <div className="bg-white flex gap-4 items-center p-4 rounded-3xl shadow-lg h-32">
          <div className="align-middle my-auto">
            <div className="bg-orange-100 rounded-full">
              <Avatar
                bg={AvatarBg}
                size="lg"
                icon={AvatarIcon}
              />
            </div>
          </div>
          <div className='space-y-0.5'>
              <p className='text-xs text-gray-400 '>{FirstLine}</p>
              <h6 className='text-2xl font-bold text-gray-800' >{SecondLine}</h6>
              <p className='text-gray-400 text-xs'><span className='text-green-600 font-bold'>{PercentCriteria === "UP" ? <AiOutlineArrowUp size={'14px'} color='green' className='inline-block my-auto mb-1'/> : <AiOutlineArrowDown size={'14px'} color='red' className='inline-block my-auto mb-1'/>}{Percentage}</span> this month</p>
          </div>
        </div>
    </div>
  )
}

export default StatCard